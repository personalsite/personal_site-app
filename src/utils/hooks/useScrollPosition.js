import { useState, useEffect } from "react"

// export const useScrollPosition = () => {
//   const [scrollPosition, setScrollPosition] = useState(getPosition)
//   const isClient = typeof document === "object"
//
//   function getPosition() {
//     return isClient
//       ? document.documentElement.scrollTop || document.body.scrollTop
//       : undefined
//   }
//
//   function handleScrollPosition() {
//     setScrollPosition(getPosition());
//   }
//
//   useEffect(() => {
//     if (!isClient) {
//       return false
//     }
//     window.addEventListener("scroll", handleScrollPosition)
//     return function clean() {
//       window.removeEventListener("scroll", handleScrollPosition);
//     };
//   }, []) // on mount && unmount
//
//   return scrollPosition || getPosition()
// }

export default function useScrollPosition() {
  const isClient = typeof document === "object";
  const [scrollPosition, setScrollPosition] = useState(0);

  useEffect(() => {
    if (!isClient) return false;

    function handleScrollPosition() {
      const newPos = isClient ? document.documentElement.scrollTop || document.body.scrollTop : undefined
      setScrollPosition(newPos);
    }

    window.addEventListener("scroll", handleScrollPosition)
    return function clean() {
      window.removeEventListener("scroll", handleScrollPosition);
    };
  }, []) // on mount && unmount

  return scrollPosition;
}

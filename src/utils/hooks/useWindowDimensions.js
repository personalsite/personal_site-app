import { useEffect, useState } from "react"

export default function useWindowDimensions() {

	const isClient = typeof window === 'object';

	function getSize() {
		return {
			width: isClient ? window.innerWidth : undefined,
			height: isClient ? window.innerHeight : undefined,
		};
	}
	const [size, setSize] = useState(getSize);
	function handleResize(){
		setSize(getSize());
	}

	useEffect(() => {
		if (!isClient) {
			return false;
		}
		window.addEventListener('resize', handleResize);
		return function clean(){
			window.removeEventListener('resize', handleResize)
		};
	}, [size]) // clean only size has changed

	return size || getSize();
}
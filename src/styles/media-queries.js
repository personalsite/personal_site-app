export const devices = {
  iPhone5: 320,
  iPhone6_7_8: 414,
  iPad: 768,
  iPadPro: 1024,
  mobile: 320,
  tablet: 768,
  desktop: 1024,
}

/**
 * @return {string} (min-width: [number]px)
 * @desc Returns media-query css string of kind: (min-width: [number]px). Used in styled-components
 */
export const mediaQuery = Object.keys(devices) // go through all attributes
  .reduce((acc, cur) => {
    // {}[iphone5] = .... at first iteration and goes on
    // style will be applied above the given device width
    acc[cur] = `(min-width: ${devices[cur /*iphone5*/] /*value of devices*/}px)`

    return acc // object
  }, {}) // object will be the first acc

/*
Usage:
const GridStyled = styled('div')`
  display: grid;
  grid-column-gap: 30px;
  grid-row-gap: 100px;
  grid-template-columns: ${props => props.templateColumns};
  @media ${mediaQuery.mobile} {
    background-color: red;
  }
  @media ${mediaQuery.tablet} {
    background-color: green;
  }
`
Because of the cascading nature of css,
the queries needs to be ordered from small to big device
*/

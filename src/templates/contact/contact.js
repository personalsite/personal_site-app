import React from "react"
import { graphql } from "gatsby"
import PageTitle from "../../components/PageTitle"
import Main from "../../components/Main/"
import Hero from "../../components/Hero"
import UnfoldAnimation from "../../animations/UnfoldAnimation"

export default function ContactTemplate(props) {
  const queryResult = props.data.craft.entry
  const pageHero = { ...queryResult.pageHero[0] }
  const pageTitle = queryResult.pageTitle
  return (
    <>
      <Main>
        <PageTitle pageTitle={pageTitle} />
          <div style={{ textAlign: "center" }}>
            <UnfoldAnimation duration={400} to={1} from={0.5} delay={0}>
              <Hero {...pageHero} />
            </UnfoldAnimation>
          </div>
        {/*<GrowAnimation duration={400} scale={0.5}>*/}
        {/*</GrowAnimation>*/}
      </Main>
    </>
  )
}

export const query = graphql`
  query ContactTamplate($id: [Int]!) {
    craft {
      entry(id: $id) {
        ... on Craft_Contact {
          metaTitle
          metaKeywords
          metaImage {
            url
          }
          metaDescription
          metaTitleTemplate
          metaSummary
          metaAuthor
          metaTopic

          pageTitle
          pageText {
            content
          }
          pageHero {
            ...PageHeroQuery
          }
        }
      }
    }
  }
`

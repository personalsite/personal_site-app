import React from "react"
import { graphql } from "gatsby"
import Seo from "../../components/Seo/Seo"
import Main from "../../components/Main/main"

import PageTitle from "../../components/PageTitle"

import StickyArea from "../../components/StickyArea/StickyArea"
import StickyElement from "../../components/StickyElement/StickyElement"
import TextMatrix from "../../components/TextMatrix/TextMatrix"
import SwitchTheme from "../../components/SwitchTheme"
import ScrollDownIconComponent from "../../components/ScrollDownIcon/ScrollDownIcon"
import Article from "../../components/Article/Article"
import styled from "@emotion/styled"
import { useMappedValue } from "../../utils/hooks/useMappedValue"
import { useWindowDimensions } from "../../utils/hooks"
import GrowAnimation from "../../animations/GrowAnimation"
import FadeinAnimation from "../../animations/FadeinAnimation"
import { H3Styled } from "../../components/Hero/styled/styled"
import MainButton from "../../components/MainButton/MainButton"
import TechStack from "../../components/TechStack/TechStack"
import GifText from "../../components/GifText/gifText"
import CodeView from "../../components/Code/Code"

const DynamicOpacityStyled = styled.div`
  opacity: ${props => props.opacity};
`
const HomeHero = ({ heroBackgroundUrl, heroTitle, heroSubtitle }) => {
  const windowHeight = useWindowDimensions().height
  const mappedOpacityTitle = useMappedValue(1, 0, 350, windowHeight)
  // const mappedFontSize = useMapValue(7, 10, 10, windowHeight + 200)
  const mappedOpacitySubtitle = useMappedValue(1, 0, 300, 600)
  return (
    <StickyArea height={`150vh`}>
      <StickyElement top={`25vh`}>
        <FadeinAnimation duration={1000} delay={1400} from={0} to={1}>
          <GrowAnimation duration={1000} delay={1200} from={1.2} to={1}>
            <DynamicOpacityStyled opacity={mappedOpacityTitle}>
              <h2
                style={{
                  marginBottom: 0,
                  fontSize: "10rem",
                }}
              >
                <GifText text={heroTitle} url={heroBackgroundUrl} />
              </h2>
              <DynamicOpacityStyled opacity={mappedOpacitySubtitle}>
                <H3Styled fontSize={"2.3rem"}>{heroSubtitle}</H3Styled>
              </DynamicOpacityStyled>
            </DynamicOpacityStyled>
          </GrowAnimation>
        </FadeinAnimation>
      </StickyElement>
    </StickyArea>
  )
}
const H4 = styled.h4`
  font-size: 2.2rem;
  font-weight: 600;
  line-height: 1;
  margin-bottom: 1.4rem;
`
const CenterSection = styled.section`
  text-align: center;
  margin-bottom: 1.4rem;
  margin-top: 1.4rem;
`
const CenterLaylout = styled.div`
  width: 66%;
  max-width: 520px;
  display: inline-block;
  margin-right: auto;
  margin-left: auto;
`
const Subtitle = styled.p`
  font-size: 1.6rem;
  line-height: 1;
`
const CenterText = ({ title, text, subtitle }) => {
  return (
    <CenterSection>
      <CenterLaylout>
        <H4>{title}</H4>
        {/*<Subtitle>{subtitle}</Subtitle>*/}
        <p>{text}</p>
      </CenterLaylout>
    </CenterSection>
  )
}

export default function HomeTemplate(props) {
  const craft = props && props.data && props.data.craft
  const { entry } = craft
  const {
    metaDescription,
    metaImage,
    metaKeywords,
    metaTitle,
    metaTitleTemplate,
  } = entry

  const heroSubtitle = entry.pageHero[0].heroSubtitle
  const heroTitle = entry.pageHero[0].heroTitle
  const heroBackgroundUrl = entry.pageHero[0].heroBackgroundUrl

  const { textMatrix } = entry
  const texts = textMatrix.map((text, idx) => ({
    title: text.textMatrixParagraphTitle,
    paragraphs: [text.textMatrixParagraphText.content],
  }))

  return (
    <>
      <Seo
        pageContextSeo={{
          metaDescription,
          metaImage,
          metaKeywords,
          metaTitle,
          metaTitleTemplate,
        }}
      />
      <SwitchTheme name={"default"}>
        <Main>
          <PageTitle pageTitle={entry.pageTitle} />
          <HomeHero
            heroBackgroundUrl={heroBackgroundUrl}
            heroTitle={heroTitle}
            heroSubtitle={heroSubtitle}
          />
          <ScrollDownIconComponent fadeAt={10} timeout={1000} />
          <TechStack />
          <MainButton>mail@stanislavpanchenko.de</MainButton>
          <TextMatrix
            staggerChildren={200}
            // atPosition={1000}
            texts={[...texts]}
          />

          <CodeView title={"useCode.js"} code={""} open={true} />
          <CodeView title={"useWindowSize.js"} code={""} open={false} />
          <CodeView title={"useScroll.js"} code={""} open={false} />

          <CenterText
            title={"Distinctio doloremque ea"}
            subtitle={
              "Adipisci, assumenda atque cumque doloribus ducimus esse eum."
            }
            text={
              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aperiam aut autem cupiditate dicta, dignissimos eaque eligendi error eum facere fuga maxime minus odit omnis optio repudiandae, sed totam, veritatis!"
            }
          />
          <Article pageArticle={[...entry.pageArticle]} />
        </Main>
      </SwitchTheme>
    </>
  )
}

export const query = graphql`
  query HomeTamplate($id: [Int]!) {
    craft {
      entry(id: $id) {
        ... on Craft_Home {
          metaTitle
          metaKeywords
          metaImage {
            url
          }
          metaDescription
          metaTitleTemplate
          metaSummary
          metaAuthor
          metaTopic

          pageTitle
          pageText {
            content
          }
          pageHero {
            ...PageHeroQuery
          }
          textMatrix {
            ... on Craft_TextMatrixTextMatrixParagraph {
              textMatrixParagraphText {
                content
              }
              textMatrixParagraphTitle
            }
          }
          pageArticle {
            ...ArticleQuery
          }
        }
      }
    }
  }
`

import React from "react"
import styled from "@emotion/styled"
import { TextH1Styled, TextH2Styled } from "../TextMatrix/styled"
import BottomLine from "../TextMatrix/BottomLine"
import CustomHTMLParser from "../CustomHTMLParser/CustomHTMLParser"
import * as uuid from 'uuid';
import { graphql } from "gatsby"


const SectionStyled = styled.section`
  padding-top: 5vh;
  padding-bottom: 15vh;
`
const AuthorName = styled.div`
  font-size: 1.5rem;
  font-weight: 600;
`
const AuthorPosition = styled.div`
  font-style: italic;
`
const LabelStyled = styled.div`
  color: #888;
  font-size: 0.75rem;
  font-weight: 600;
  margin-top: 0.3rem;
`

const Author = ({ name, position }) => {
  return (
    <>
      <div>
        <AuthorPosition>{position}</AuthorPosition>
        <AuthorName>{name}</AuthorName>
        <LabelStyled>March 17, 2019</LabelStyled>
      </div>
    </>
  )
}

const FooterStyled = styled.footer`
  padding-top: 3vh;
`

const ArticleStyled = styled.article`
  max-width: 570px;
  margin-left: auto;
  margin-right: auto;
`

const html = `
		<h3>
					Amet, atque autem blanditiis excepturi.
		</h3>
		<p>
          Dolor minima, placeat? Blanditiis debitis dicta enim explicabo facere
          iusto, labore necessitatibus non nulla odit, officia quod reiciendis
          suscipit velit voluptatem voluptates. Dicta esse illo mollitia
          quisquam voluptatibus. A ad adipisci aperiam cum delectus dicta
          eligendi et maxime optio, qui quia quisquam rerum voluptatem? Ab
          accusantium ad, amet architecto asperiores atque aut doloremque, eaque
          ipsam iusto nostrum placeat repudiandae sapiente sint soluta
          temporibus unde velit vero? Ad animi asperiores beatae distinctio ea
          eveniet explicabo, fugiat maxime, non nulla provident tempora! Aperiam
          dicta est nisi praesentium sapiente temporibus veritatis! Error et
          modi nulla? Alias atque ducimus eos explicabo illo molestiae, mollitia
          nulla, numquam odio possimus quibusdam, sit vel veritatis vero
          voluptatem.
     </p>
     <p>
          Dolor minima, placeat? Blanditiis debitis dicta enim explicabo facere
          iusto, labore necessitatibus non nulla odit, officia quod reiciendis
          suscipit velit voluptatem voluptates. Dicta esse illo mollitia
          quisquam voluptatibus. A ad adipisci aperiam cum delectus dicta
          eligendi et maxime optio, qui quia quisquam rerum voluptatem? Ab
          accusantium ad, amet architecto asperiores atque aut doloremque, eaque
          ipsam iusto nostrum placeat repudiandae sapiente sint soluta
          temporibus unde velit vero? Ad animi asperiores beatae distinctio ea
          eveniet explicabo, fugiat maxime, non nulla provident tempora! Aperiam
          dicta est nisi praesentium sapiente temporibus veritatis! Error et
          modi nulla? Alias atque ducimus eos explicabo illo molestiae, mollitia
          nulla, numquam odio possimus quibusdam, sit vel veritatis vero
          voluptatem.
     </p>
	`
const PageText = ({ htmlText, title }) => {
  const P = styled.p``
  const H3 = styled.h3`
    line-height: 1.20849;
    font-weight: 500;
    letter-spacing: 0.015em;
    font-size: 1.5rem;
    margin-bottom: 1rem;
  `
  const customComponents = [
    {
      name: "p",
      Component: P,
    },
    {
      name: "h3",
      Component: H3,
    },
  ]
  return (
    <div
      style={{
        marginTop: "55px",
        marginBottom: "55px",
      }}
    >
      {
        title
          ? <H3>{title}</H3>
          : null
      }
      <CustomHTMLParser html={htmlText} customComponents={customComponents} />
    </div>
  )
}

const ArticleHead = ({title, subtitle, decor}) => {
  const SubtitleStyled = styled.p`
    font-size: 1.7rem;
    font-weight: 400;
    color: #888888;
  `;
  return(
    <header style={{ marginBottom: "2rem" }}>
      <TextH2Styled>{decor}</TextH2Styled>
      <TextH1Styled>
        {title}
      </TextH1Styled>
      <SubtitleStyled>
        {subtitle}
      </SubtitleStyled>
      <BottomLine />
    </header>
  )
}

export default function Article({pageArticle}){
  return (
    <SectionStyled>
      <ArticleStyled>
        {
          pageArticle.map(entry => {
            const componentType = entry.__typename.replace('Craft_PageArticlePage', '');
            if(componentType === 'ArticleHead'){
              return(
                <ArticleHead
                  key={uuid()}
                  title={entry.pageArticleTitle}
                  decor={entry.pageArticleDecor}
                  subtitle={entry.pageArticleSubtitle}
                />
              )
            } else if (componentType === 'ArticleTextBlock'){
              return (
                <PageText
                  key={uuid()}
                  htmlText={entry.pageArticleContentParagraph.content}
                  title={entry.pageArticleContentTitle}/>
              )
            }
            return(
              <span>s</span>
            )
          })
        }
      </ArticleStyled>
    </SectionStyled>
  )
}
export const query = graphql`
  fragment ArticleQuery on Craft_PageArticleUnion {
    ... on Craft_PageArticlePageArticleHead {
      pageArticleDecor
      pageArticleSubtitle
      pageArticleTitle
    }
    ... on Craft_PageArticlePageArticleTextBlock {
      pageArticleContentTitle
      pageArticleContentParagraph {
        content
      }
    }
  }
`
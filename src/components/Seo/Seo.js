import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import { Helmet } from "react-helmet"

export default function Seo(props) {
  const queryData = useStaticQuery(graphql`
    query SeoQuery {
      craft {
        globals {
          defaultSeo: seo {
            keywords: metaKeywords
            title: metaTitle
            description: metaDescription
            titleTemplate: metaTitleTemplate
            summary: metaSummary
            author: metaAuthor
            topic: metaTopic
            image: metaImage {
              url
            }
            
          }
        }
      }
    }
  `)

  const { pageContextSeo } = props
  const { defaultSeo } = queryData.craft.globals

  const meta = [
    {
      name: `description`,
      content: pageContextSeo.metaDescription
        ? pageContextSeo.metaDescription
        : defaultSeo.description,
    },
    {
      property: `og:title`,
      content: defaultSeo.title,
    },
    {
      name: `keywords`,
      content: pageContextSeo.metaKeywords
        ? pageContextSeo.metaKeywords
        : defaultSeo.keywords,
    },
    {
      name: `twitter:creator`,
      content: pageContextSeo.metaAuthor
        ? pageContextSeo.metaAuthor
        : defaultSeo.author,
    },
    {
      name: `twitter:title`,
      content: pageContextSeo.metaTitle?pageContextSeo.metaTitle:defaultSeo.title,
    },
    {
      name: `twitter:description`,
      content: pageContextSeo.metaDescription
        ? pageContextSeo.metaDescription
        : defaultSeo.description,
    },
    {
      name: "copyright",
      content: "company name",
    },
    {
      name: "topic",
      content: "",
    },
    {
      name: "summary",
      content: pageContextSeo.metaSummary
        ? pageContextSeo.metaSummary
        : defaultSeo.summary,
    },
  ]
  return (
    <Helmet
      title={
        pageContextSeo.metaTitle ? pageContextSeo.metaTitle : defaultSeo.title
      }
      titleTemplate={
        pageContextSeo.titleTemplate
          ? pageContextSeo.titleTemplate
          : defaultSeo.titleTemplate
      }
      meta={meta}
    />
  )
}

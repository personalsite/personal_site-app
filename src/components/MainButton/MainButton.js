import React, { useRef } from "react"
import styled from "@emotion/styled"
import SwitchTheme from "../SwitchTheme"
import useIsInView from "../../utils/hooks/useIsinView"


const MainButtonStyled = styled.button`
  background-color: #272727;
  color: #e0e0e0;
  border: none;
  outline: none;
  border-radius: 0.4rem;
  box-shadow: ${props => props.theme.shadow.prime};
  box-shadow: ${props => props.theme.shadow.second};
  
  font-size: .85rem;
  font-weight: 500;
  
  padding: 1.2rem 2.4rem 1.2rem 2.4rem;
  cursor: pointer;
  transition: transform 200ms, box-shadow 200ms;
  &:hover {
    transform: scale(.986);
    box-shadow: 0 3px 7px 0px #0000004d;
  };
`;
const MainButton = ({ children, onClick }) => {
  const ref = useRef(null);
  const inView = useIsInView(ref);
  return (
    <SwitchTheme name={'default'}>
      <MainButtonStyled ref={ref} onClick={onClick}>{children}</MainButtonStyled>
    </SwitchTheme>
  )
}

export default MainButton

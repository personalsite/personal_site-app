import React, { useState } from "react"
import styled from "@emotion/styled"

const Wrapper = styled.div`
  // background-color: rgb(21, 23, 24);
  background-color: rgb(42, 39, 52);
  width: auto;
  overflow-y: hidden;
  height: ${props => (props.closed ? 40 : 400)}px;
  will-change: height;
  border-radius: 4px;
  box-shadow: 0px 3px 10px 0px rgba(0, 0, 0, 0.55);
  transition: box-shadow 100ms, transform 200ms,
    height 400ms cubic-bezier(0.26, 0.93, 0.95, 0.81);
  transform: scale(1.002);
  padding: 5px;
  margin: 0 50px 25px 50px;
  cursor: text;
  &:hover {
    transform: scale(1);
    box-shadow: 0px 3px 5px 0px rgba(0, 0, 0, 0.25);
  }
`

const CodeArea = styled.div`
  color: white;
  -webkit-font-smoothing: antialiased;
  font-family: Hack, monospace;
  font-family: "SFMono-Regular", Consolas, "Roboto Mono", "Droid Sans Mono",
    "Liberation Mono", Menlo, Courier, monospace;
  padding: 10px;
  font-variant: contextual;
  font-variant-ligatures: contextual;
  font-feature-settings: "calt";
  line-height: 133%;
  //font-family: monospace;
`
const Pre = styled.pre`
  overflow: visible;
  white-space: pre-wrap;
  padding: 0;
  margin: 0;
  line-height: 1.42;
  font-size: 0.98rem;
  border-radius: 0;
`
const Button = styled.div`
  border: none;
  outline: none;
  width: 14px;
  height: 14px;
  margin: 5px;
  border-radius: 8px;
  cursor: pointer;
  background-color: #ffbd2e;
  display: inline-block;
  transition: transform 100ms, background-color 100ms;
  will-change: transform, background-color;
  &:hover {
    transform: scale(1.1);
    // background-color: #e0a526;
    background-color: #ffcc5e;
  }
`
const WindowHead = styled.div`
  cursor: pointer;
  display: flex;
  padding-top: 2px;
  justify-content: space-between;
`
const WindowTitle = styled.div`
  color: white;
  padding-top: 3px;
  padding-left: 26px;
`

const WindowName = styled.div`
  color: #898598;
  padding-top: 3px;
  margin-right: 5px;
  display: inline-block;
`
const Word = styled.span`
  //margin-right: 4px;
  //margin-left: 4px;
  font-size: 1rem;
  &:first-of-type {
    margin-left: 0;
  }
`
const Operator = styled(Word)`
  color: #9fca56;
`
const Type = styled(Word)`
  color: #e6cd69;
`
const Def = styled(Word)`
  color: #55b5db;
`
const Var = styled(Word)`
  color: #a074c4;
`
const Attribute = styled(Word)`
  color: #3fd61a;
`
const String = styled(Word)`
  color: #55b5db;
`
const Number = styled(Word)`
  color: #cd3f45;
`

export default function CodeView({ title, code, open }) {
  const [closed, setClosed] = useState(!open)
  return (
    <Wrapper closed={closed}>
      <WindowHead onClick={() => setClosed(!closed)}>
        <Button onClick={() => setClosed(!closed)} />
        <WindowTitle>{title}</WindowTitle>
        <WindowName>{"<code/>"}</WindowName>
      </WindowHead>
      <CodeArea>
        <Pre>
          <code>
            <Type>const </Type>
            <Def>pluckDeep </Def>
            <Operator>= </Operator>
            <Def>key </Def>
            <Operator>=> </Operator>
            <Def>obj </Def>
            <Operator>=> </Operator>
            <Var>key</Var>.<Attribute>split</Attribute>(<String>'.'</String>)
            {".reduce((accum, key) => accum[key], obj)"}
            {
              "const compose = (...fns) => res => fns.reduce((accum, next) => next(accum), res)"
            }
            {"const unfold = (f, seed) => {"}
          </code>
        </Pre>
      </CodeArea>
    </Wrapper>
  )
}

import React from "react"
import styled from "@emotion/styled"
import LinkColumn from "../LinkColumn/LinkColumn"
import { LayoutStyled } from "../Layout/styled"
import CopyRight from "../Copyright/Copyright"

const FooterStyled = styled.footer`
  height: 50vh;
  background-color: #fbfcfd;
  padding-top: 5vh;
  padding-bottom: 5vh;
  position: relative;
`

const Column = styled.div`
  display: inline-block;
`
const ColumnsWrapper = styled.div`
  display: flex;
  justify-content: space-evenly;
`
const Footer = () => {
  const title = "Pages"
  const items = [
    {
      value: "Home",
      url: "/",
    },
    {
      value: "Contact",
      url: "/",
    },
    {
      value: "Technology",
      url: "/",
    },
    {
      value: "About",
      url: "/",
    },
    {
      value: "AGB",
      url: "/",
    },
    {
      value: "Home",
      url: "/",
    },
  ]

  return (
    <FooterStyled>
      <LayoutStyled>
        <ColumnsWrapper>
          <Column>
            <LinkColumn title={title} items={[...items]} />
          </Column>
          <Column>
            <LinkColumn title={title} items={[...items]} />
          </Column>
          <Column>
            <LinkColumn title={title} items={[...items]} />
          </Column>
          <Column>
            <LinkColumn title={title} items={[...items]} />
          </Column>
        </ColumnsWrapper>
      </LayoutStyled>
      <CopyRight />
    </FooterStyled>
  )
}

export default Footer

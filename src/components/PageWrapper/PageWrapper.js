import React from 'react';
import NavigationHeader from "../NavigationHeader"
import Layout from "../layout"
import styled from "@emotion/styled"
// import styled from "styled-components";
import { GrowAnimation } from "../../animations"

const PageGradientStyled = styled.div`
	position: fixed;
	bottom: 0;
	height: 1px;
	left: 0;
	right: 0;
	&:after{
		height: 75px;
		display: block;
		background: linear-gradient(0deg, rgba(255,255,255,1) 0%, rgba(255,255,255,0.01724439775910369) 100%);
		right: 0;
		left: 0;
		position: relative;
		top: -75px;
		content: ' ';
	}
`;
export default function PageWrapper({children}) {
	return(
		<div>
			{/*<NavigationHeader/>*/}
			<Layout>
				{children}
			</Layout>
			<PageGradientStyled/>
		</div>
	)
};

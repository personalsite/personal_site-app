import React from 'react';

import { ThemeProvider } from 'emotion-theming';

import themeDefault from '../../Themes/Default';



export default function SwitchTheme({ name, children }) {
  const inDev = process.env.NODE_ENV === 'development';
  const themeType = {
    default: themeDefault,
  };
  if (!(name in themeType) && inDev) {
    console.warn(`This theme does not exist!: ${name}`);
  }
  const theme = name in themeType ? themeType[name] : {};
  return (
    <ThemeProvider
      theme={theme}
    >
      {children}
    </ThemeProvider>
  );
}


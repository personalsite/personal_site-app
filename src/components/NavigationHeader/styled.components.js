import styled from "@emotion/styled"
import { mediaQuery } from "../../styles/media-queries"

export const NavStyled = styled.nav`
	display: flex;
	justify-content: space-between;
	align-items: center;
	height: 100%;
	position: relative;
	a {
		font-size: .96rem;
		margin: 0 15px 0 15px;
		font-weight: 400;
		color: #3c3c3c;
		text-decoration: none;
		&:first-of-type{
			margin-left: 0;
		}
		&:last-of-type{
			margin-right: 0;
		}
	}
`
export const HeaderStyled = styled.header`
  height: 60px;
  position: fixed;
  z-index: 100;
  left: 0;
  right: 0;
  top: 0;
  padding-top: 19px;
  border-bottom: 1px solid #efefef;
  background-color: #ffffff;
`
export const LinkStyled = styled.a`
	&:hover{
		color: ${props => props.theme.colors.accent};
	}
`
export const LayoutStyled = styled("div")`
  display: block;
  margin-left: auto;
  margin-right: auto;
  @media ${mediaQuery.mobile} {
    width: 95%;
  }
  @media ${mediaQuery.tablet} {
    width: 80%;
  }
  @media ${mediaQuery.desktop} {
    width: 75%;
  }
`;
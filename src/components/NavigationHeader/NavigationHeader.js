import React from "react"
import { graphql, useStaticQuery } from "gatsby"
import * as uuid from "uuid"
import {
  HeaderStyled,
  LayoutStyled,
  LinkStyled,
  NavStyled,
} from "./styled.components"
import { useScrollPosition } from "../../utils/hooks/"
import posed from "react-pose"
import useWindowDimensions from "../../utils/hooks/useWindowDimensions"
import SwitchTheme from "../SwitchTheme"

const HoverEffect = posed.div({
  hoverable: true,
  init: {
    boxShadow: "0px 1px 3px 0px rgba(0,0,0,0)",
  },
  hidden: {
    opacity: 0.9,
  },
  visible: {
    opacity: 1,
    height: 60,
    zIndex: 100,
    left: 0,
    top: 0,
    position: "fixed",
    right: 0,

    transition: {
      duration: 200,
    },
  },
  hover: {
    opacity: .97,
    boxShadow: "0px 1px 3px 0px rgba(0,0,0,0.13)",
    transition: {
      duration: 200,
    },
  },
})

const HeaderLink = ({ href, text }) => {
  return (
    <SwitchTheme name={"default"}>
      <LinkStyled href={href}>{text}</LinkStyled>
    </SwitchTheme>
  )
}

export default function NavigationHeader() {
  const scrollPos = useScrollPosition()
  const windowDimension = useWindowDimensions()

  const queryResult = useStaticQuery(graphql`
    query NavigationHeaderQuery {
      craft {
        entries(section: navigationHeader) {
          ... on Craft_NavigationHeader {
            navHeaderLink {
              url
              customText
              entry {
                uri
              }
            }
          }
        }
      }
    }
  `)
  const baseUrl = `${process.env.GATSBY_URL_BASE_SITE}/`
  const { entries } = queryResult.craft
  return (
    <HoverEffect
      pose={scrollPos > windowDimension.height / 3 ? "hidden" : "visible"}
    >
      <HeaderStyled>
        <LayoutStyled>
          <NavStyled>
            <div>
              <a style={{ color: "blue", fontWeight: 600 }} href="/">
                Stanislav
              </a>
            </div>

            <div>
              {entries.map(({ navHeaderLink }) => (
                <HeaderLink
                  key={uuid()}
                  href={baseUrl + navHeaderLink.entry.uri}
                  text={navHeaderLink.customText}
                />
              ))}
            </div>
          </NavStyled>
        </LayoutStyled>
      </HeaderStyled>
    </HoverEffect>
  )
}

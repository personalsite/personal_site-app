import React from "react"
import { graphql } from "gatsby"
import {
  H2Styled,
  H3Styled,
} from "./styled/styled"
import styled from "@emotion/styled"
import SwitchTheme from "../SwitchTheme"

const Wrapper = styled.div`
  display: inline-block;
  text-align: center;
`;
export default function Hero({ heroTitle, heroSubtitle, heroBackgroundUrl, fontSize }) {
  return (
    <Wrapper>
      <SwitchTheme name={'default'}>
        <H2Styled fontSize={fontSize} url={heroBackgroundUrl}>{heroTitle}</H2Styled>
        <H3Styled>{heroSubtitle}</H3Styled>
      </SwitchTheme>
    </Wrapper>
  )
}


export const query = graphql`
  fragment PageHeroQuery on Craft_PageHeroPageHero {
    heroBackgroundUrl
    heroTitle
    heroSubtitle
  }
`

import styled from "@emotion/styled";

export const H2Styled = styled('h2')({
	fontSize: '7.3rem',
	fontWeight: 900,
	lineHeight: 1,
	margin: 0,
	letterSpacing: '3px',
	willChange: 'font-size'
},props => ({
	fontSize: props.fontSize,
	background: `url(${props.url}) no-repeat`,
	backgroundPosition: 'center',
	WebkitBackgroundClip: 'text',
	WebkitTextFillColor: 'transparent',
}));

export const H3Styled = styled.h3`
	font-size: ${props => props.fontSize || '1.4rem'};
	color: ${props => props.theme.colors.prime};
	font-weight: 900;
	line-height: 1;
	margin: 0;
	letter-spacing: 3px;
`;

export const HeroWrapperStyled = styled.div`
	height: 200vh;
	text-align: center;
`;

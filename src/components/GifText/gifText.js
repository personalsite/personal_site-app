import React from 'react';
import styled from "@emotion/styled";

const Styled = styled('span')({
  fontWeight: 900,
  lineHeight: 1,
  margin: 0,
  letterSpacing: '3px',
  willChange: 'font-size',
},props => ({
  // fontSize: props.fontSize ? props.fontSize : null,
  background: `url(${props.url}) no-repeat`,
  backgroundPosition: 'center',
  WebkitBackgroundClip: 'text',
  WebkitTextFillColor: 'transparent',
  backgroundSize: 'cover',
}));

export default function GifText({text, url, fontSize}) {
  return (
    <Styled fontSize={fontSize} url={url}>
      {text}
    </Styled>
  )
}
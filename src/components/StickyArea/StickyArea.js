import styled from "@emotion/styled"
import React from "react"

const Style = styled.div`
	height: ${props =>props.height};
	will-change: height;
`
const StickyArea = ({ children, height }) => {
	return <Style height={height}>{children}</Style>
}
export default StickyArea;
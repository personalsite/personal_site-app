import styled from "@emotion/styled"

const H2 = styled.h2`
  font-size: 42px;
`;

export default H2;
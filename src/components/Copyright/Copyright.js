import React from "react"
import styled from "@emotion/styled"
import { LayoutStyled } from "../Layout/styled"

const Aside = styled.aside`
  display: flex;
  color: ${props => props.theme.colors.third};
  font-size: 0.75rem;
  justify-content: center;
  > div {
    margin-right: 15px;
  }
  border-top: 1px solid ${props => props.theme.colors.border};
  padding-top: 10px;
`
const ToBottom = styled.div`
  position: absolute;
  bottom: 10px;
  left: 0;
  right: 0;
`
export default function CopyRight(props) {
  return (
    <ToBottom>
      <LayoutStyled>
        <Aside>
          <div>Stanislav Panchenko</div>
          <div>Copyright: Stanislav Panchenko</div>
          <div>Code License: MIT</div>
          <div>2018 - 2019</div>
        </Aside>
      </LayoutStyled>
    </ToBottom>
  )
}

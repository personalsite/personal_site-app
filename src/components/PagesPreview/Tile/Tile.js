import styled from "@emotion/styled"
import React from "react"
import posed, { PoseGroup } from "react-pose"

const Tile = styled.div`
  width: 350px;
  height: 200px;
  background-color: white;
  color: black;
  display: inline-block;
  margin: 13px;
  margin-right: 50px;
  overflow: hidden;
  &:first-of-type {
    margin-left: 50px;
  }
  border-radius: 7px;
  will-change: transform, opacity;
`
const AnimatedTile = posed(Tile)({
  enter: {
    opacity: 1,
    scale: 1,
    transition: {
      opacity: { ease: "easeOut", duration: 400 },
      scale: { ease: "easeOut", duration: 400 },
    },
  },
  hoverable: true,
  hover: {
    scale: 1.07,
    transition: {
      scale: { ease: "easeOut", duration: 200 },
    },
  },
  exit: {
    opacity: 0,
  },
  init: {
    opacity: 0,
    scale: 0.7,
  },
})
const TilesContainerStyle = styled.div`
  overflow-x: scroll;
  overflow-y: hidden;
  white-space: nowrap;
  margin-bottom: -35px; // hide scroll bar
`
const TilesAnimationStagger = posed(TilesContainerStyle)({
  enter: {
    staggerChildren: 100,
  },
  exit: {
    staggerChildren: 100,
  },
})

export const TilesContainer = ({ children, onTileHover }) => {
  return (
    <PoseGroup animateOnMount={true}>
      <TilesAnimationStagger posed={"visible"} key={"animation-stagger"}>
        {children &&
          children.map((child, idx) => {
            return (
              <AnimatedTile
                onMouseEnter={() => onTileHover(child)}
                key={`page-tile-${idx}`}
              >
                {child}
              </AnimatedTile>
            )
          })}
      </TilesAnimationStagger>
    </PoseGroup>
  )
}

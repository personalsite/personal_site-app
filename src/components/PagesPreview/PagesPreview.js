import React, { forwardRef, useState } from "react"
import styled from "@emotion/styled"
import posed, { PoseGroup } from "react-pose"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faGripHorizontal } from "@fortawesome/free-solid-svg-icons"
import { TilesContainer } from "./Tile/Tile"
import { PreviewTitle } from "./PreviewTitle"
import { graphql, StaticQuery } from "gatsby"
import GifText from "../GifText/gifText"

const ButtonStyled = styled.button`
  background-color: black;
  position: fixed;
  right: 30px;
  bottom: 50px;
  width: 55px;
  height: 55px;
  border-color: #676767;
  border-radius: 30px;
  cursor: pointer;
  outline: none;
  color: white;
  z-index: 10;
  box-shadow: ${props => props.theme.shadow.prime};
  box-shadow: ${props => props.theme.shadow.second};
  transition: transform 200ms;
  will-change: transform;
  &:hover {
    transform: scale(0.96);
  }
`
export const TitleTitle = styled.div`
  text-align: center;
  font-size: 4rem;
  font-weight: 600;
  margin-top: 3rem;
`

const PagesButton = ({ onClick }) => {
  return (
    <ButtonStyled onClick={onClick}>
      <FontAwesomeIcon icon={faGripHorizontal} />
    </ButtonStyled>
  )
}

const PagePreviewBG = styled.div`
  position: fixed;
  z-index: 100;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  will-change: opacity;
  background-color: black;
  color: white;
`

const CenterOnPage = styled.div`
  display: flex;
  align-items: center;
  height: 100vh;
`
// tiles: [{tileTitle: string, previewTitle: string, url: string }]
const PagePreview = forwardRef(({ onClick, children }, ref) => {
  const [hoveredTileName, setHoveredTileName] = useState("All Pages")
  return (
    <PagePreviewBG onClick={onClick} ref={ref}>
      <CenterOnPage>
        <div
          onMouseLeave={() => setHoveredTileName("all pages")}
          style={{
            overflow: "hidden",
            paddingBottom: "20px",
          }}
        >
          <PreviewTitle title={hoveredTileName} />
          <TilesContainer
            onTileHover={child => setHoveredTileName(child.props.name)}
          >
            {children}
          </TilesContainer>
        </div>
      </CenterOnPage>
    </PagePreviewBG>
  )
})
const AnimatedTilesContainer = posed(PagePreview)({
  enter: {
    opacity: 1,
  },
  exit: {
    opacity: 0,
  },
  init: {
    opacity: 0,
  },
})
const Tile = styled.div``
function PagesPreview(props) {
  const [visible, setVisible] = useState(false)
  const urls = [...props.data.craft.entries]
  const parsedLinks = urls
    .map(({ navHeaderLink }) => {
      return navHeaderLink
        ? {
            text: navHeaderLink.text,
            url: navHeaderLink.entry.uri,
          }
        : null
    })
    .filter(link => /* filter null */ link)
  const tiles = parsedLinks.map(({ text, url }) => ({
    pageTitle: text,
    pageUrl: url,
    gifUrl: "https://i.giphy.com/media/3o6ZtpKpayDeYGV0Uo/source.gif",
  }))

  return (
    <div id={"PagesPreviewComponent"}>
      <PagesButton onClick={() => setVisible(true)} />
      <PoseGroup animateOnMount={true}>
        {visible ? (
          <AnimatedTilesContainer
            onClick={() => setVisible(false)}
            key={"PagePreview"}
          >
            {tiles.map(({ pageTitle, gifUrl }, idx) => (
              <Tile key={`tile-content-${idx}`} name={pageTitle}>
                <TitleTitle>
                  <GifText text={pageTitle} fontSize={"3rem"} url={gifUrl} />
                </TitleTitle>
              </Tile>
            ))}
          </AnimatedTilesContainer>
        ) : null}
      </PoseGroup>
    </div>
  )
}

export default props => (
  <StaticQuery
    query={graphql`
      query {
        craft {
          entries {
            ... on Craft_NavigationHeader {
              navHeaderLink {
                text
                entry {
                  uri
                }
              }
            }
          }
        }
      }
    `}
    render={data => <PagesPreview data={data} {...props} />}
  />
)

import styled from "@emotion/styled"
import React from "react"
import posed, { PoseGroup } from "react-pose"

const PreviewTitleStyled = styled.div`
  font-size: 2rem;
  width: 300px;
  //margin-left: 5rem;
  //margin-bottom: 5rem;
  will-change: opacity, transform;
  text-transform: uppercase;
  font-weight: 900;
  position: absolute;
  left: 10vw;
  top: 0;
`;

const AnimatedTitle = posed(PreviewTitleStyled)({
  enter: {
    x: '0%',
    opacity: 1,
    transition: {
      opacity: { ease: 'easeOut', duration: 300},
      x: { ease: 'easeOut', duration: 300},
    }
  },
  exit: {
    x: '-20%',
    opacity: 0,
    transition: {
      opacity: { ease: 'easeOut', duration: 300},
      x: { ease: 'easeOut', duration: 300},
    }
  },
  init: {
    x: '-50%',
    opacity: 0,
  }
})

export const PreviewTitle = ({title}) => {
  return(
    <div
      style={{
        position: 'relative',
        height: '60px'
      }}
    >
      <PoseGroup animateOnMount={true}>
        <AnimatedTitle key={`preview-tile-title-${title}`}>
          {title}
        </AnimatedTitle>
      </PoseGroup>
    </div>
  )
}
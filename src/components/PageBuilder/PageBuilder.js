import React from 'react';
import { graphql } from 'gatsby';
import * as pageBuilderComponent from './components';
import * as uuid from 'uuid';

export default function PageBuilder(props) {
	const pageData = [...props.pageBuilder];
	return(
		<>
			{
				pageData.map(comp => {
					const compName = comp.__typename.replace('Craft_PageBuilderPage', '');
					const ResolvedComponent = pageBuilderComponent[compName];
					return (
						ResolvedComponent
							? <ResolvedComponent
								{...comp} key={uuid()}
							/>
							: null
					)
				})
			}
		</>
	)
};

// export const query = graphql`
//     fragment PageBuilderQuery on Craft_PageBuilderUnion {
// 			__typename
//         ... on Craft_PageBuilderPageTitle {
//             ...PageBuilderPageTitleQuery
//         }
// 		}
// `;
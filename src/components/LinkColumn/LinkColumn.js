import React from "react"
import styled from "@emotion/styled"

const Li = styled.li`
  list-style: none;
  margin-bottom: 10px;
  & a {
    font-size: 0.8rem;
    color: ${props => props.theme.colors.second || "#565656"};
    text-decoration: none;
    &:hover {
      color: blue;
    }
  }
`
const H4 = styled.h4`
  margin-bottom: 10px;
`
const Ul = styled.ul`
  margin-left: 0px;
`
const Nav = styled.nav`
  margin: 10px;
`

export default function LinkColumn({ title, items }) {
  return (
    <Nav>
      <H4>{title}</H4>
      <Ul>
        {items.map(({ value, url }, idx) => {
          return (
            <Li key={`link-${value}-${idx}`}>
              <a href={url}>{value}</a>
            </Li>
          )
        })}
      </Ul>
    </Nav>
  )
}

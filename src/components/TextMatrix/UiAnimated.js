import posed from "react-pose"
import { useScrollPosition } from "../../utils/hooks"
import React from "react"

const UlAnimation = posed.ul({
	hidden: {
		// time children
		staggerChildren: ({stagger}) => stagger
	},
	visible: {
		// time children
		staggerChildren: ({stagger}) => stagger
	},
	init: {
		// list style
		display: 'flex',
		flexDirection: 'row',
		margin: 0,
		padding: 0,
		flexWrap: 'wrap',
		justifyContent: 'space-between',
	}
});
const UlAnimated = ({children, animate, staggerChildren}) => {
	// const scrollPosition = useScrollPosition()
	return(
		<UlAnimation
			pose={animate ? 'visible': 'hidden'}
			stagger={staggerChildren}>
			{children}
		</UlAnimation>
	)
};
export default UlAnimated;
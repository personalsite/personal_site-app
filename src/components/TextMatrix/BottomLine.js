import posed from "react-pose"
import { useScrollPosition } from "../../utils/hooks"
import React from "react"

const BottomLineAnimation = posed.div({
	visible: {
		width: '100%'
	},
	hidden: {
		width: '0%'
	},
	init:{
		position: 'relative',
		height: 1,
		backgroundColor: '#eaeaea'
	}
})
const BottomLine = ({animateAt}) => {
	const scrollPosition = useScrollPosition();
	const animPos = animateAt ? animateAt : 0;
	return(
		<BottomLineAnimation pose={scrollPosition >= animPos ? 'visible': 'hidden'}/>
	)
}
export default BottomLine;
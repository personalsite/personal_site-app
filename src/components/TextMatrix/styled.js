import styled from "@emotion/styled"
import { mediaQuery } from "../../styles/media-queries"

export const TextH2Styled = styled.h2`
  font-size: 1.3rem;
  margin: 0;
  font-weight: 500;
  padding: 0;
  color: ${props => props.theme.colors.accent};
`
export const TextH1Styled = styled.h1`
  line-height: 1.08365;
  font-weight: 600;
  letter-spacing: -0.003em;
  font-size: 3rem;
  color: ${props => props.theme.colors.prime};
`
export const TextMatrixHeadStyled = styled.header`
  margin-bottom: 3rem;
`

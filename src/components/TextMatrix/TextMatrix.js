import React, { useRef } from "react"
import Text from "./Text"
import {
  TextH1Styled,
  TextH2Styled,
  TextMatrixHeadStyled,
} from "./styled"
import posed from "react-pose"
import UlAnimated from "./UiAnimated"
import './ListAnimated.style.css';
import useIsinView from "../../utils/hooks/useIsinView"



const LiAnimated = posed.li({
	visible: {
		y: 0,
		opacity: 1,
		transition: ({duration}) => (
			{
				opacity: { ease: 'easeOut', duration: duration},
				y: { ease: 'easeOut', duration: duration },
			}
		)
	},
	hidden: {
		y: 75,
		opacity: 0,
		transition: ({duration}) => (
			{
				opacity: { ease: 'easeOut', duration: duration},
				y: { ease: 'easeOut', duration: duration },
			}
		)
	},
});


export default function TextMatrix({children, texts}) {
	const ref = useRef(null)
	const animate = useIsinView(ref);
  return (
    <section ref={ref}>
      <TextMatrixHeadStyled>
        <TextH2Styled>Text Upper Title</TextH2Styled>
        <TextH1Styled>Text Under Title</TextH1Styled>
      </TextMatrixHeadStyled>

        <UlAnimated animate={animate} staggerChildren={200} >
					{
						texts.map((text, idx) => (
							<LiAnimated
								className={'ListAnimated'}
								key={`listItem-${idx}`}
								duration={700}>
								<Text title={text.title} paragraphs={[...text.paragraphs]}/>
							</LiAnimated>
						))
					}
        </UlAnimated>

    </section>
  )
}

import React from "react"
import styled from "@emotion/styled"
import * as uuid from "uuid"
import CustomHTMLParser from "../CustomHTMLParser/CustomHTMLParser"

const TextH3 = styled.h3`
  line-height: 1.20849;
  font-weight: 500;
  letter-spacing: 0.015em;
  font-size: 1.5rem;
  margin-bottom: .6rem;
`

const BoldStyled = styled.strong`
  color: ${props => props.theme.colors.accent};
`

const P = styled.p`
  color: ${props => props.theme.colors.text};
`

const Em = styled.em`
  color: ${props => props.theme.colors.accent};
`

const Ul = styled.ul`
  margin: 0;
  padding: 0;
`

const A = styled.a`
  color: ${props => props.theme.colors.accent};
`
const customElements = [
  {
    name: "a",
    Component: A,
  },
  {
    name: "p",
    Component: P,
  },
  {
    name: "strong",
    Component: BoldStyled,
  },
  {
    name: "em",
    Component: Em,
  },
]

const Text = ({ title, paragraphs }) => {
  return (
    <article>
      <TextH3>{title}</TextH3>
      {paragraphs.map((html, idx) => {
        return (
          <span key={uuid()}>
            <CustomHTMLParser html={html} customComponents={customElements} />
          </span>
        )
      })}
    </article>
  )
}

export default Text

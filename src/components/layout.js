import React from "react";
import styled from "@emotion/styled";
import { mediaQuery } from "../styles/media-queries";



const LayoutStyled = styled('div')`
  display: block;
  margin-left: auto;
  margin-right: auto;  
  @media ${mediaQuery.mobile} {
   width: 95%;
  }
  @media ${mediaQuery.tablet} {
   width: 80%;
  }
  @media ${mediaQuery.desktop} {
    width: 75%;  
  }
`;

export default function Layout ({ children }) {
  return(
    <div>
      <LayoutStyled className={'grid'}>
        {children}
      </LayoutStyled>
    </div>
    )
};
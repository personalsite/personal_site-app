import styled from "@emotion/styled"
import React from "react"

const Style = styled.div`
    position: sticky;
    top: ${props => props.top};
    text-align: center;
    will-change: top;
  `
const StickyElement = ({ children, top }) => {
	return <Style top={top}>{children}</Style>
};
export default StickyElement;
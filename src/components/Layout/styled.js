import styled from "@emotion/styled"
import { mediaQuery } from "../../styles/media-queries"

export const LayoutStyled = styled('div')`
  display: block;
  margin-left: auto;
  margin-right: auto;  
  @media ${mediaQuery.mobile} {
   width: 95%;
  }
  @media ${mediaQuery.tablet} {
   width: 80%;
  }
  @media ${mediaQuery.desktop} {
    width: 66%;
    max-width: 980px;  
  }
`;
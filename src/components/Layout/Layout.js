import React from "react"
import { Global } from "@emotion/core"
import { globalStyle } from "../../styles/glogal.styles"
import NavigationHeader from "../NavigationHeader"
import FadeinAnimation from "../../animations/FadeinAnimation"
import { LayoutStyled } from "./styled"
import SwitchTheme from "../SwitchTheme"
import styled from "@emotion/styled"
import PagesPreview from "../PagesPreview/PagesPreview"
import Footer from "../Footer/Footer"

const PageGradientStyled = styled.div`
  position: fixed;
  bottom: 0;
  height: 0px;
  left: 0;
  right: 0;
  &:after {
    height: 50px;
    display: block;
    background: linear-gradient(
      0deg,
      rgba(255, 255, 255, 1) 0%,
      rgba(255, 255, 255, 0.01724439775910369) 100%
    );
    right: 0;
    left: 0;
    position: relative;
    top: -50px;
    content: " ";
  }
`
const Layout = ({ children }) => {
  return (
    <SwitchTheme name={"default"}>
      {/*injects styles to every page created*/}
      <FadeinAnimation duration={700} from={0} to={1} delay={0}>
        <Global styles={globalStyle} />
        <NavigationHeader />
        <LayoutStyled>
          {children}

          <SwitchTheme />
        </LayoutStyled>
      </FadeinAnimation>
      {/*<PageGradientStyled />*/}
      <PagesPreview />
      <Footer />
    </SwitchTheme>
  )
}

export default Layout

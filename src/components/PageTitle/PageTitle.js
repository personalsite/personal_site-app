import React from 'react';
import { H1Styled } from "./styled.components"
import {SlideRightAnimation} from '../../animations'
import SwitchTheme from "../SwitchTheme"
import FadeinAnimation from "../../animations/FadeinAnimation"


export default function PageTitle({pageTitle}) {
	return (
		<SlideRightAnimation duration={700} from={'-20%'} to={'0%'} delay={1000}>
			<SwitchTheme name={'default'}>
				<FadeinAnimation duration={1200} delay={1000} from={0} to={1}>
					<H1Styled>
							{pageTitle}
					</H1Styled>
				</FadeinAnimation>
			</SwitchTheme>
		</SlideRightAnimation>
	)
};
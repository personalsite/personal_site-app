import styled from "@emotion/styled"


export const H1Styled = styled.h1`
	margin: 0;
	position: relative;
	top: 5vh;
	font-size: 1.4rem;
	font-weight: 600;
	color: ${props => props.theme.colors.prime};
`;
import React from "react"
import { CSSTransition } from "react-transition-group"
import "./scroll-down-icon-component.style.scss"
import { useScrollPosition } from "../../utils/hooks"


const ScrollDownIconComponent = ({ fadeAt, timeout }) => {
  const scrollPos = useScrollPosition();
  return (
    <CSSTransition
      in={scrollPos <= fadeAt}
      appear
      classNames={"mouse-container-animation"}
      timeout={timeout}
    >
      <div
        className={["mouse-container",
          // fade ? 'hidden' : ''
        ].join(" ")}
      >
        <div className="mouse">
          <span className="scroll-down" />
        </div>
      </div>
    </CSSTransition>
  )
}

export default ScrollDownIconComponent

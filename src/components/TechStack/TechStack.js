import React, { forwardRef, useRef } from "react"
import { useHover, useInView } from "../../utils/hooks"
import styled from "@emotion/styled"
import posed from "react-pose"
import { H2 } from "../Titles"
import Tooltip from "../Tooltip"
const techIcons = [
  {
    name: "React",
    width: 80,
    height: 80,
    url: "https://reactjs.org/",
    src: "https://upload.wikimedia.org/wikipedia/commons/a/a7/React-icon.svg",
  },
  {
    name: "NestJs",
    width: 48,
    height: 48,
    url: "https://nestjs.com",
    src:
      "https://seeklogo.com/images/N/nestjs-logo-09342F76C0-seeklogo.com.png",
  },
  {
    name: "GraphQL",
    width: 55,
    height: 55,
    url: "https://graphql.org/",
    src: "https://upload.wikimedia.org/wikipedia/commons/1/17/GraphQL_Logo.svg",
  },
  {
    name: "Styled-Components",
    width: 55,
    height: 55,
    url: "https://www.styled-components.com/",
    src: "https://angeloocana.com/imgs/styled-components.png",
  },
  {
    name: "CSS",
    width: 55,
    height: 55,
    url: "https://developer.mozilla.org/en-US/docs/Web/CSS",
    src:
      "https://upload.wikimedia.org/wikipedia/commons/d/d5/CSS3_logo_and_wordmark.svg",
  },
  {
    name: "HTML",
    width: 55,
    height: 55,
    url: "https://www.w3.org/html/",
    src:
      "https://upload.wikimedia.org/wikipedia/commons/6/61/HTML5_logo_and_wordmark.svg",
  },
]


const Section = styled.section`
  padding-bottom: 20vh;
  min-height: 100vh;
  display: flex;
  justify-content: center;
  align-content: center;
`

const ChildStaggger = posed.div({
  visible: {
    staggerChildren: 100,
  },
  hidden: {
    staggerChildren: 10,
  },
  init: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-evenly",
    marginBottom: "20vh",
  },
})

const IconWrapper = styled.div`
  margin: 5px;
`
const Img = styled.img`
  transition: transform 120ms;
  cursor: pointer;
  margin: 0;
  &:hover {
    transform: scale(1.1);
  }
`
const TooltipName = styled.div`
  font-size: 14px;
  margin: 0;
  font-weight: 500;
`;
const TooltipUrl = styled.div`
  font-size: 12px;
  margin: 0;
  opacity: .5;
  font-weight: 400;
`;
const Icon = forwardRef(({ icon }, ref) => {
  // forwardRef for AnimationIcons component
  const { src, width, height, url, name } = icon
  const {event, bind} = useHover();
  const pos = event.target && event.target.getBoundingClientRect();
  return (
    <IconWrapper {...bind} ref={ref}>
      <Tooltip
        visible={event.hovered}
        left={pos && (pos.left + (pos.width / 2))}
        top={pos && pos.top}
      >
        <TooltipName>{name}</TooltipName>
        <TooltipUrl>{url}</TooltipUrl>
      </Tooltip>
      <a href={url}>
        <Img width={`${width}px`} height={`${height}px`} src={src} alt="" />
      </a>
    </IconWrapper>
  )
})
const AnimatedIcon = posed(Icon)({
  visible: {
    opacity: 1,
    scale: 1,
    transition: {
      duration: 250,
    },
  },
  hidden: {
    opacity: 0,
    scale: 0.2,
    transition: {
      duration: 10,
    },
  },
});

const TechWrapper = styled.div`
  text-align: center;
`
const PCentered = styled.p`
  width: 66%;
  max-width: 690px;
  margin-left: auto;
  margin-right: auto;
  font-size: 22px;
  line-height: 1.335;
  color: gray;
`
const TechIntro = () => {
  return (
    <div>
      <H2>My Tools</H2>
      <PCentered>
        Every tool serves a certain purpose and solves a specific problem. Those
        are my tools which help me solving mine.
      </PCentered>
    </div>
  )
}
const Tech = props => {
  const ref = useRef(null)
  const animate = useInView(ref)

  return (
    <TechWrapper>
      <ChildStaggger ref={ref} pose={animate ? "visible" : "hidden"}>
        {techIcons.map((icon, idx) => (
          <AnimatedIcon key={`tech-icon-${idx}`} icon={icon} />
        ))}
      </ChildStaggger>
      <TechIntro />
    </TechWrapper>
  )
}

export default function TechStack(props) {
  return (
    <Section>
      <Tech />
    </Section>
  )
}

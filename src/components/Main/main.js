import React from 'react';


export default function Main({children}) {
	return(
		<main style={{marginTop: '60px'}} className={'main'}>{children}</main>
	)
};

module.exports = {
	siteMetadata: {
		title: 'Home Page',
		siteTitle: 'Stanislavs Home Page',
		siteUrl: 'https://localhost.de', // No trailing slash (used to create xml sitemap)
		description: 'Stanisalvs personal home page',
		author: `Stanislav Panchenko`,
	},
	webfontconfig: {
		google: {
			families: ['Open Sans:400,700', 'Montserrat:400,600,700,900'],
		},
	},
}
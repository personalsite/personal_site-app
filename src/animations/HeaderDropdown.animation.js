import posed, { PoseGroup } from "react-pose"
import * as uuid from "uuid"
import React from "react"

const HeaderDropdownAnimation = ({ children, duration }) => {
	const AnimationStyle = posed.div({
		enter: {
			y: "0px",
			transition: {
				default: {
					duration: duration,
					ease: "linear",
				},
			},
		},
		exit: {
			position: 'fixed',
			top: 0,
			left: 0,
			right: 0,
			borderBottom: '1px solid #efefef',
			backgroundColor: '#fffffff2',
			zIndex: 100,
			y: "-60px",
		},
	})
	return (
		<PoseGroup >
			<AnimationStyle key={uuid()}>{children}</AnimationStyle>
		</PoseGroup>
	)
};
export default HeaderDropdownAnimation;
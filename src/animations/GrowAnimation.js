import React, { useEffect, useState } from "react"
import posed, { PoseGroup } from "react-pose"
import * as uuid from "uuid"
import styled from "@emotion/styled"
import useTimeout from "../utils/hooks/useTimeout"
import useSignal from "../utils/hooks/useSignal"


const Style = styled.div`
	transform: ${props => `scale(${props.scale})`};
	transition: transform ${props => props.duration}ms;
	will-change: transform;
`;
const AnimationComponent = ({ children, duration, from, to, delay }) => {
  const animate = useSignal(delay);
	return (
		<Style duration={duration} scale={animate ? to : from}>
			{children}
		</Style>
	)
}
export default AnimationComponent

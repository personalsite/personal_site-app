import React from 'react';
import styled from "@emotion/styled"
import useSignal from "../utils/hooks/useSignal"



const Style = styled.div`
	transform: translate(${props => props.transform});
	transition: transform ${props => props.duration}ms;
	will-change: transform;
`
const AnimationComponent = ({ children, delay, duration, from, to}) => {
	const animate = useSignal(delay);
	return (
			<Style transform={animate ? to : from} duration={duration}>
				{children}
			</Style>

	)
}
export default AnimationComponent;
import React from "react"
import styled from "@emotion/styled"
import useSignal from "../utils/hooks/useSignal"


const Style = styled.div`
  transition: opacity ${props => props.duration}ms;
  opacity: ${props => props.opacity};
  will-change: opacity;
`;
const AnimationComponent = ({ children, duration, delay, from, to }) => {
  const animate = useSignal(delay);
  return (
    <Style duration={duration} opacity={animate ? to : from}>{children}</Style>
  )
}
export default AnimationComponent;

import React from "react"
import styled from "@emotion/styled"
import useSignal from "../utils/hooks/useSignal"


const Style = styled.div`
  transition: transform ${props => props.duration}ms;
  transform: scaleY(${props => props.scale});
  will-change: transform;
`;
const AnimationComponent = ({ children, duration, delay, from, to }) => {
  const animate = useSignal(delay);

  return (
    <Style duration={duration} scale={animate ? to : from}>
      {children}
    </Style>

  )
}

export default AnimationComponent

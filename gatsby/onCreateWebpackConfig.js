module.exports = ({
  stage,
  rules,
  loaders,
  plugins,
  actions,
}) => {
  actions.setWebpackConfig({
    module: {
      rules: [
        {
          test:   /\.html$/,
          use: [
            'html-loader',

          ]
        },
      ],
    },
    plugins: [
    ],
  })
}

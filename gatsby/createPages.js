const path = require("path")
const fs = require("fs")

module.exports = async function({ actions, graphql }) {
	const { createPage } = actions
	const query = `{
      craft {
        entries {
          uri
          id
          type {
            handle
          }
          section {
            handle
          }
        }
      }
    }`
	const queryResult = await graphql(query).catch(err => {
		console.error(err);
	});

	const {entries} = queryResult.data.craft;

	entries.forEach(entry => {
		// Check for corresponding template.
		const templateFile = path.resolve(
			`src/templates/${entry.section.handle}/${entry.type.handle}.js`,
		);
		// Only process the entry if there is a template for it.
		if (fs.existsSync(templateFile)) {
			const {uri, id} = entry;
			const baseUrl = `${process.env.GATSBY_URL_BASE_SITE}/` || '/';
			const path = uri || '/';
			console.log('-----------------------------');
			console.info(`Create Page: ${baseUrl}${path === '/'? '': path}`);
			console.log('-----------------------------');

			const withPageData = {
				context: {
					id,
					baseUrl,
					pageType: 'page',
				},
				path,
				component: templateFile,
			};
			createPage({...withPageData});
		}
	});
}